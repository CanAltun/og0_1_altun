package promocode;

public class Promocode {
	public static int findPromoCode(long[] zahlen, long suchwert) {
		long links = 0, rechts = zahlen.length - 1;
		while (suchwert >= zahlen[(int) links] && suchwert <= zahlen[(int) rechts]) {
			long anzElemente = zahlen[(int) rechts] - zahlen[(int) links];
			long position = links + (long) (((double) rechts - links) * (suchwert - zahlen[(int) links]) / anzElemente);
			if (suchwert > zahlen[(int) position]) {
				links = position + 1;
			} else if (suchwert < zahlen[(int) position]) {
				rechts = position - 1;
			} else {
				return (int) position;
			}
		}
		return -1;
	}
}
