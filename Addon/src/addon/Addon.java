package addon;

public class Addon {

	// Attribute
	private int idNummer;
	private double preis;
	private int bestand;
	private int maxBestand;
	private String bezeichner;
	private char kaufen;
	private char verkaufen;

	// Standard-Konstruktor
	public Addon() {
	}

	// Verwaltungsmethoden
	public int getidNummer() {
		return idNummer;
	}

	public void setidNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getbestand() {
		return bestand;
	}

	public void setbestand(int bestand) {
		this.bestand = bestand;
	}

	public int getmaxBestand() {
		return maxBestand;
	}

	public void setmaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}

	public String getbezeichner() {
		return bezeichner;
	}

	public void setbezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}

	public char getkaufen() {
		return kaufen;
	}

	public void setkaufen(char kaufen) {
		this.kaufen = kaufen;
	}

	public char getverkaufen() {
		return verkaufen;
	}

	public void setverkaufen(char verkaufen) {
		this.verkaufen = verkaufen;

	}
}
