package Primzahl;

import java.util.Scanner;

public class Primzahl {
	
	public static void main(String[] args) {
		System.out.println("Geben sie ihre zahl ein");
		Scanner scan = new Scanner(System.in);
		long zahl = scan.nextLong();
		long time = System.currentTimeMillis();
		System.out.println(" Ist eine Primzahl:" + istprimzahl(zahl));
		System.out.println(System.currentTimeMillis() - time + "ms");
		scan.close();
	}
	
	public static boolean istprimzahl(long zahl) {
		boolean primzahl = true;

		for (long i = zahl - 1; i > 1; i--) {
			int rest = (int) (zahl % i);

			if (rest == 0) {
				i = 0;
				primzahl = false;
			}

		}
		return primzahl;
	}

}
